**WARNING**: This code is in development and does not work yet!

# QSetup

This package tries to automate the process of setting up a measurement PC as far as reasonable.

1. Install Python. For example like this `winget install Python.Python.3.11`
2. Install git. For example like this `winget install Git.Git`
3. Install QSetup. `python -m pip install https://git-ce.rwth-aachen.de/qutech/qsetup`



## Software environment(s)

qsetup will setup a completely tracked python environment for you. It uses git submodules for that.

1. Select a name for your setup. It should be something like `IrisAutotuneQuasar` i.e. `{Setup}{Project}{Experiment}`
2. `python -m qsetup.software init IrisAutotuneQuasar`
   - This will query you for a personal access token for git-ce.
   - This will create a new folder `IrisAutotuneQuasar` in `C:\Users\USER\Documents` and a corresponding project on git-ce
   - The folder will be populated with a setup_config
3. Execute `python -m qsetup.software populate IrisAutotuneQuasar`
