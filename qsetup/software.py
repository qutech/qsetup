import pathlib
import subprocess
from dataclasses import dataclass
from pathlib import Path
import warnings
import sys
import argparse

import gitlab

RWTH_GITLAB_CE = "https://git-ce.rwth-aachen.de"
SETUPS_URL = f"{RWTH_GITLAB_CE}/qutech-test/setups"


@dataclass
class GitLabConfig:
    url: str
    setup_group: str
    admin_token: str

    developer_group: str
    developer_token: str | None

    def create_setup_project(self, name: str):
        gl = gitlab.Gitlab(self.url, private_token=self.admin_token)
        gl.auth()

        group, = gl.groups.get(search=self.setup_group)
        group = gl.groups.get(group.id)

        projects = group.projects.list(search=name)
        if projects:
            project, = projects
            warnings.warn("Project already exists")
        else:
            project = gl.projects.create(
                {'name': name, 'namespace_id': group.id})
        return project.http_url_to_repo

    def create_developer_token(self, name: str):
        gl = gitlab.Gitlab(self.url, private_token=self.admin_token)
        gl.auth()

        group, = gl.groups.get(search=self.developer_group)
        group = gl.groups.get(group.id)

        try:
            token = group.access_tokens.create({
                'name': f"{name}-dev",
                "scopes": ["api"],
                "expires_at": "",
                "role": "developer",
            })
            self.developer_token = token.token
        except gitlab.exceptions.GitlabError as e:
            raise RuntimeError(f"The token does not allow creating another token. "
                               f"It must be a personal access token for that purpose") from e


@dataclass
class SetupConfig:
    name: str
    location: Path
    git_packages: list[str]

    gitlab_config: GitLabConfig


def prepare_atsaverage():
    subprocess.check_call()



def create_software_environment(config: SetupConfig):
    if not config.location.exists():
        raise FileNotFoundError(config.location)

    base_location = config.location / config.name
    try:
        base_location.mkdir()
    except FileExistsError:
        warnings.warn(f"{config.location} already exists")

    config.gitlab_config.create_developer_token(config.name)

    setup_git_url = config.gitlab_config.create_setup_project(config.name)

    venv_dir = base_location / "venvs"
    venv_dir.mkdir(exist_ok=True)
    venv_gitignore = venv_dir / ".gitignore"
    if not venv_gitignore.exists():
        venv_gitignore.write_text("*\n")

    subprocess.check_call(["git", "init"])
    subprocess.check_call(["git", "remote", "add", "origin", setup_git_url])

    for url in config.git_packages:
        subprocess.check_call(["git", "submodule", "add", url])
    subprocess.check_call(["git", "submodule", "update"])

    default_venv = venv_dir / "default"
    subprocess.check_call(["python", "-m", "venv", default_venv])

    for d in pathlib.Path(base_location).glob("*/.git"):
        d.absolute()
        subprocess.call([default_venv / "Scripts" / "activate", "&&", f"python -m pip install -e {d.parent.absolute()}"], shell=True)


if __name__ == "__main__":
    location = Path(__file__).parent / "test_setup"
    location.mkdir(exist_ok=True)

    gitlab_config = GitLabConfig(
        url=RWTH_GITLAB_CE,
        developer_group="qutech-test",
        setup_group="qutech-test/setups",
        admin_token="glpat-ZK6HLBRQJxzLF2KsVDyT",
        developer_token=None,
    )

    config = SetupConfig(
        name="TestPlsDelete",
        location=location,
        git_packages=["https://git-ce.rwth-aachen.de/qutech-test/someproject.git"],
        gitlab_config=gitlab_config,
    )
    create_software_environment(config)











